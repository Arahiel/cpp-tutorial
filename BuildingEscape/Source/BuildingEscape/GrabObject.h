// Copyright MrArahiel 2020

#pragma once

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"
#include "GrabObject.generated.h"


UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class BUILDINGESCAPE_API UGrabObject : public UActorComponent
{
	GENERATED_BODY()

public:	
	// Sets default values for this component's properties
	UGrabObject();
	// Called every frame
	virtual void TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction) override;
	TArray<AActor*> GetTracingActors() const;

protected:
	// Called when the game starts
	virtual void BeginPlay() override;

private:	



	UPROPERTY(EditAnywhere, Meta = (ToolTip = "Actors, which grab the owner"))
		TArray<AActor*> ActorsThatCanGrab;
		
};
