// Copyright MrArahiel 2020


#include "GrabObject.h"
#include <Runtime\Engine\Public\DrawDebugHelpers.h>

// Sets default values for this component's properties
UGrabObject::UGrabObject()
{
	// Set this component to be initialized when the game starts, and to be ticked every frame.  You can turn these features
	// off to improve performance if you don't need them.
	PrimaryComponentTick.bCanEverTick = true;

	// ...
}


// Called when the game starts
void UGrabObject::BeginPlay()
{
	Super::BeginPlay();

	auto PlayerPawn = GetWorld()->GetFirstPlayerController()->GetPawn();
	ActorsThatCanGrab.Add(PlayerPawn);

}


// Called every frame
void UGrabObject::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);

	GetTracingActors();
}

TArray<AActor*> UGrabObject::GetTracingActors() const
{
	TArray<AActor*> Output;
	auto Owner = GetOwner();
	for (auto actor : ActorsThatCanGrab)
	{
		FHitResult HitResult;
		auto Start = actor->GetActorLocation();

		auto ForwardVector = actor->GetActorForwardVector();
		auto End = (ForwardVector * 200.f) + Start;
		FCollisionQueryParams CollisionParams;

		DrawDebugLine(GetWorld(), Start, End, FColor::Green, false, 1, 0, 5);
		if (Owner->ActorLineTraceSingle(HitResult, Start, End, ECollisionChannel::ECC_Pawn, CollisionParams))
		{
			Output.Add(actor);
			UE_LOG(LogTemp, Warning, TEXT("%s is tracing %s"), *(actor->GetName()), *(Owner->GetName()));
		}
	}

	return Output;
}

