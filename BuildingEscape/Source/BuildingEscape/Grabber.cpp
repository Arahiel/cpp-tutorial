// Copyright MrArahiel 2020


#include "Grabber.h"
#include "Grabbable.h"
#include "GameFramework/PlayerController.h"
#include <Runtime\Engine\Public\DrawDebugHelpers.h>

#define OUT 

// Sets default values for this component's properties
UGrabber::UGrabber()
{
	// Set this component to be initialized when the game starts, and to be ticked every frame.  You can turn these features
	// off to improve performance if you don't need them.
	PrimaryComponentTick.bCanEverTick = true;

	// ...
}


// Called when the game starts
void UGrabber::BeginPlay()
{
	Super::BeginPlay();
	
	SetRequiredComponents();
	BindActions();
}

void UGrabber::SetRequiredComponents()
{
	auto Owner = GetOwner();
	PhysicsHandle = Owner->FindComponentByClass<UPhysicsHandleComponent>();
	if (!PhysicsHandle)
	{
		UE_LOG(LogTemp, Error, TEXT("PhysicsHandle was not found on %s!"), *GetOwner()->GetName());
	}

	InputComponent = Owner->InputComponent;
}

void UGrabber::BindActions()
{
	if (InputComponent)
	{
		InputComponent->BindAction("Grab", EInputEvent::IE_Pressed, this, &UGrabber::Grab);
		InputComponent->BindAction("Grab", EInputEvent::IE_Released, this, &UGrabber::Release);
	}
	else
	{
		UE_LOG(LogTemp, Error, TEXT("Input component was not found!"));
	}
}

void UGrabber::Grab()
{
	UE_LOG(LogTemp, Warning, TEXT("Grab action executed"));

	SetHandleLocation();
	auto HitResult = GetFirstActorHit();
	SetHeldActor(HitResult);
}

void UGrabber::SetHeldActor(FHitResult& HitResult)
{
	auto ActorHit = HitResult.GetActor();
	if (ActorHit)
	{
		if (ActorHit->FindComponentByClass<UGrabbable>())
		{
			BindHeldActorToHandle(HitResult.GetComponent());
			UE_LOG(LogTemp, Warning, TEXT("Actor held: %s"), *(ActorHit->GetName()));
		}
	}
}

void UGrabber::BindHeldActorToHandle(UPrimitiveComponent* Component)
{
	PhysicsHandle->GrabComponent(Component, NAME_None, HandleLocation, true);
}

FHitResult UGrabber::GetFirstActorHit() const
{
	FHitResult HitResult;

	auto End = (Rotation.Vector() * Reach) + Location;

	//Pierwszy spos�b na line trace - pojedynczy kana� - W Collision Responses (Collision Details aktora) widzimy jakie kana�y blokuje ten aktor
	//World->LineTraceSingleByChannel(OUT HitResult, Location, End, ECollisionChannel::ECC_Visibility); // ECC_Pawn powoduje kolizj� z DefaultPawn!

	//Drugi spos�b na line trace - wiele mo�liwych kana��w
	FCollisionObjectQueryParams ObjectsToHit;
	ObjectsToHit.AddObjectTypesToQuery(ECC_PhysicsBody);
	//ObjectsToHit.AddObjectTypesToQuery(ECC_WorldDynamic); // Przyk�ad

	// Z tutoriala - niepotrzebne, poniewa� Default_Pawn_BP u�yty przez nas nie jest typu PhysicsBody.
	// W przypadku pierwszego sposobu, nasz Pawn nie blokuje Visibility channel wi�c w obu przypadkach jest zb�dne
	// Domy�lne parametry s� wystarczaj�ce (jest to opcjonalny parametr)
	FCollisionQueryParams TraceParams(FName(TEXT("")), false, GetOwner());

	GetWorld()->LineTraceSingleByObjectType(OUT HitResult, Location, End, ObjectsToHit, TraceParams);
	return HitResult;
}

void UGrabber::Release()
{
	UE_LOG(LogTemp, Warning, TEXT("Release action executed"));
	PhysicsHandle->ReleaseComponent();
}

// Called every frame
void UGrabber::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);

	MoveHeldActorToHandleLocation();
}

void UGrabber::SetHandleLocation()
{
	GetPlayerViewPort();
	HandleLocation = Location + Rotation.Vector() * GrabReach;
}

void UGrabber::GetPlayerViewPort()
{
	auto World = GetWorld();
	auto PlayerController = World->GetFirstPlayerController();

	PlayerController->GetPlayerViewPoint(OUT Location, OUT Rotation);
}

void UGrabber::MoveHeldActorToHandleLocation()
{
	if (PhysicsHandle->GrabbedComponent)
	{
		SetHandleLocation();
		PhysicsHandle->SetTargetLocation(HandleLocation);
	}
}

