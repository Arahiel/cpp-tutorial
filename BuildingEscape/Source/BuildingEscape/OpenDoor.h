// Copyright MrArahiel 2020

#pragma once

#define nameof(x) #x

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"
#include "../../Intermediate/ProjectFiles/DoorOpenDirection.h"
#include <Runtime\Engine\Classes\Engine\TriggerVolume.h>
#include "Components/AudioComponent.h"

#include "OpenDoor.generated.h"


UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class BUILDINGESCAPE_API UOpenDoor : public UActorComponent
{
	GENERATED_BODY()

public:	
	// Sets default values for this component's properties
	UOpenDoor();
	// Called every frame
	virtual void TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction) override;
	void SetDoorTargetYaw(DoorOpenDirection direction);
	void ResetDoorYaw();
	void CheckTriggers() const;
	void AddTriggerActor(AActor* actor);

	UPROPERTY(BlueprintReadWrite, Meta = (ToolTip = "Current target door yaw", UIMin = "0.0", ClampMin = "0.0", UIMax = "90.0", ClampMax = "90.0"))
		float TargetYaw;

protected:
	// Called when the game starts
	virtual void BeginPlay() override;


private:	
	float DefaultRotation;
	float ActualRotation;
	float OpenTimeStamp;

	void OpenDoor(float DeltaTime);
	void SetDoorRotation(float rotation);
	void CheckTrigger(const AActor* trigger, const FString& name) const;
	bool IsOverlappedByAnyActorsThatOpen(const ATriggerVolume* triggerVolume) const;
	void PlaySound() const;
	void SetRequiredComponents();

	UPROPERTY()
		UAudioComponent* AudioComponent = nullptr;
	UPROPERTY(EditAnywhere, Meta = (ToolTip = "Delay after which door closes", UIMin ="0.0", ClampMin = "0.0"))
		float DoorCloseDelay = 2.0f;
	UPROPERTY(EditAnywhere, Meta = (ToolTip = "Speed in which door opens and closes", UIMin = "0.0", ClampMin = "0.0"))
		float DoorOpenSpeed = 2.0f;
	UPROPERTY(EditAnywhere, Meta = (ToolTip = "Maximum door open angle", UIMin = "0.0", ClampMin = "0.0", UIMax = "90.0", ClampMax = "90.0"))
		float MaxAngle = 90.0f;
	UPROPERTY(EditAnywhere, Meta = (ToolTip = "Trigger volume to open door in forward direction"))
		ATriggerVolume* ForwardTriggerVolume;
	UPROPERTY(EditAnywhere, Meta = (ToolTip = "Trigger volume to open door in backward direction"))
		ATriggerVolume* BackwardTriggerVolume;
	UPROPERTY(EditAnywhere, Meta = (ToolTip = "Actors, which trigger the door"))
		TArray<AActor*> ActorsThatOpen;
};
