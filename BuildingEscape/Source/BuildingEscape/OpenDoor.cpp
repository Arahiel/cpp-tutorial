// Copyright MrArahiel 2020


#include "OpenDoor.h"

// Sets default values for this component's properties
UOpenDoor::UOpenDoor()
{
	// Set this component to be initialized when the game starts, and to be ticked every frame.  You can turn these features
	// off to improve performance if you don't need them.
	PrimaryComponentTick.bCanEverTick = true;

	// ...
}


// Called when the game starts
void UOpenDoor::BeginPlay()
{
	Super::BeginPlay();
	SetRequiredComponents();
}

void UOpenDoor::SetRequiredComponents()
{
	DefaultRotation = GetOwner()->GetActorRotation().Yaw;
	ActualRotation = DefaultRotation;
	TargetYaw = DefaultRotation;
	AudioComponent = GetOwner()->FindComponentByClass<UAudioComponent>();

	auto PlayerPawn = GetWorld()->GetFirstPlayerController()->GetPawn();
	AddTriggerActor(PlayerPawn);
	CheckTriggers();
}

void UOpenDoor::PlaySound() const
{
	if (AudioComponent)
	{
		AudioComponent->Play();
	}
	else
	{
		UE_LOG(LogTemp, Error, TEXT("Audio component was not found!"));
	}
}

// Called every frame
void UOpenDoor::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);

	if (ForwardTriggerVolume && IsOverlappedByAnyActorsThatOpen(ForwardTriggerVolume))
	{
		if (TargetYaw == DefaultRotation)
		{
			SetDoorTargetYaw(DoorOpenDirection::Forward);
		}
		OpenTimeStamp = GetWorld()->GetTimeSeconds();
	}
	
	if (BackwardTriggerVolume && IsOverlappedByAnyActorsThatOpen(BackwardTriggerVolume))
	{
		if (TargetYaw == DefaultRotation)
		{
			SetDoorTargetYaw(DoorOpenDirection::Backward);
		}
		OpenTimeStamp = GetWorld()->GetTimeSeconds();
	}

	if(ForwardTriggerVolume && BackwardTriggerVolume 
		&& !IsOverlappedByAnyActorsThatOpen(ForwardTriggerVolume) && TargetYaw != DefaultRotation
		&& !IsOverlappedByAnyActorsThatOpen(BackwardTriggerVolume) && TargetYaw != DefaultRotation)
	{
		auto ActualTime = GetWorld()->GetTimeSeconds();
		if (ActualTime - OpenTimeStamp >= DoorCloseDelay)
		{
			ResetDoorYaw();
		}
	}

	OpenDoor(DeltaTime);
}

void UOpenDoor::OpenDoor(float DeltaTime)
{
	ActualRotation = FMath::FInterpTo(ActualRotation, TargetYaw, DeltaTime, DoorOpenSpeed);
	SetDoorRotation(ActualRotation);
}

void UOpenDoor::SetDoorTargetYaw(DoorOpenDirection direction)
{
	PlaySound();
	switch (direction)
	{
		case DoorOpenDirection::Forward:
			TargetYaw = DefaultRotation + MaxAngle;
		break;

		case DoorOpenDirection::Backward:
			TargetYaw = DefaultRotation - MaxAngle;
		break;
	}
}

void UOpenDoor::ResetDoorYaw()
{
	PlaySound();
	TargetYaw = DefaultRotation;
}

void UOpenDoor::SetDoorRotation(float rotation)
{
	auto Owner = GetOwner();
	auto Rotator = Owner->GetActorRotation();

	Rotator.Yaw = rotation;
	Owner->SetActorRotation(Rotator);
}

void UOpenDoor::CheckTriggers() const
{
	CheckTrigger(ForwardTriggerVolume, nameof(ForwardTriggerVolume));
	CheckTrigger(BackwardTriggerVolume, nameof(BackwardTriggerVolume));
}

void UOpenDoor::CheckTrigger(const AActor* trigger, const FString& name) const
{
	if (!trigger)
	{
		UE_LOG(LogTemp, Error, TEXT("%s owner has OpenDoor component added, but no %s specified!"), *(GetOwner()->GetName()), *name);
	}
}

void UOpenDoor::AddTriggerActor(AActor* actor)
{
	ActorsThatOpen.Add(actor);
}

bool UOpenDoor::IsOverlappedByAnyActorsThatOpen(const ATriggerVolume* triggerVolume) const
{
	for (auto actor : ActorsThatOpen)
	{
		if (triggerVolume->IsOverlappingActor(actor))
		{
			return true;
		}
	}
	return false;
}


