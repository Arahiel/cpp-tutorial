// Copyright MrArahiel 2020

#include "WorldPosition.h"
#include "GameFramework/Actor.h"

DEFINE_LOG_CATEGORY(MyCategory)

// Sets default values for this component's properties
UWorldPosition::UWorldPosition()
{
	// Set this component to be initialized when the game starts, and to be ticked every frame.  You can turn these features
	// off to improve performance if you don't need them.
	PrimaryComponentTick.bCanEverTick = true;

	// ...
}


// Called when the game starts
void UWorldPosition::BeginPlay()
{
	Super::BeginPlay();



	auto ObjectName = GetOwner()->GetName();
	auto ObjectPosition = GetOwner()->GetActorLocation();
	UE_LOG(MyCategory, Warning, TEXT("%s is at position X:%f, Y:%f, Z:%f"), *ObjectName, ObjectPosition.X, ObjectPosition.Y, ObjectPosition.Z);
	
}


// Called every frame
void UWorldPosition::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);

	// ...
}

