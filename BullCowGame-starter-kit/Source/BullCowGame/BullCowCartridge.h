// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Console/Cartridge.h"

#include "BullCowCartridge.generated.h"

UCLASS(ClassGroup=(Custom), meta=(BlueprintSpawnableComponent))
class BULLCOWGAME_API UBullCowCartridge : public UCartridge
{
	GENERATED_BODY()

	public:
	virtual void BeginPlay() override;
	virtual void OnInput(const FString& Input) override;
	void SetupNewGame();
	void SetHiddenWord();
	void WinMatch();
	void LoseMatch();
	int CountBulls(const FString& Input) const;
	int CountCows(const FString& Input) const;
	int LoseLife();
	bool IsBull(const TCHAR& c, int index) const;
	bool IsCow(const TCHAR& c, int indexInInput) const;
	bool IsIsogram(const FString& Input) const;

	// Your declarations go below!
	private:
		FString HiddenWord;
		int Lives;
		TArray<FString> HiddenWordsList;
};
