// Fill out your copyright notice in the Description page of Project Settings.
#include "BullCowCartridge.h"

void UBullCowCartridge::BeginPlay() // When the game starts
{
    Super::BeginPlay();
    const FString WordListPath = FPaths::ProjectContentDir() / TEXT("WordLists/HiddenWords.txt");
    FFileHelper::LoadFileToStringArrayWithPredicate(HiddenWordsList, *WordListPath, [this](const FString& Word) {return IsIsogram(Word); });
    UBullCowCartridge::SetHiddenWord();
    PrintLine(TEXT("Welcome to the Bull Cow  game."));
    SetupNewGame();
}

void UBullCowCartridge::SetHiddenWord()
{
    int RandomIndex = FMath::RandRange(0, HiddenWordsList.Num() - 1);
    UBullCowCartridge::HiddenWord = HiddenWordsList[RandomIndex];
}

void UBullCowCartridge::WinMatch()
{
    ClearScreen();
    PrintLine(TEXT("Correct!"));
    SetupNewGame();
}

void UBullCowCartridge::LoseMatch()
{
    ClearScreen();
    PrintLine(TEXT("You lost!"));
    PrintLine(TEXT("The correct word was: %s"), *HiddenWord);
    SetupNewGame();
}

bool UBullCowCartridge::IsBull(const TCHAR& c, int index) const
{
    return HiddenWord[index] == c;
}

bool UBullCowCartridge::IsCow(const TCHAR& c, int indexInInput) const
{
    int32 index;
    bool isFound = HiddenWord.FindChar(c, index);

    if (index == INDEX_NONE)
    {
        return false;
    }

    return !IsBull(c, indexInInput);
}

bool UBullCowCartridge::IsIsogram(const FString& Input) const
{
    for (int i = 0; i < Input.Len() - 1; i++)
    {
        for (int j = i + 1; j < Input.Len(); j++)
        {
            if (Input[i] == Input[j])
            {
                return false;
            }
        }
    }
    return true;
}

int UBullCowCartridge::CountBulls(const FString& Input) const
{
    int bulls = 0;
    for (int i = 0; i < Input.Len(); i++)
    {
        if (IsBull(Input[i], i))
        {
            bulls++;
        }
    }
    return bulls;
}

int UBullCowCartridge::CountCows(const FString& Input) const
{
    int cows = 0;
    
    for (int i = 0; i < Input.Len(); i++)
    {
        if (IsCow(Input[i], i))
        {
            cows++;
        }
    }
    return cows;
}

int UBullCowCartridge::LoseLife()
{
    return --Lives;
}

void UBullCowCartridge::SetupNewGame()
{
    Lives = 5;
    SetHiddenWord();
    PrintLine(TEXT("New hidden word with %i letters\nhas been set."), HiddenWord.Len());
    PrintLine(TEXT("You can start guessing"));
}

void UBullCowCartridge::OnInput(const FString& Input) // When the player hits enter
{
    ClearScreen();
    PrintLine(Input);

    if (Input.Equals(HiddenWord))
    {
        WinMatch();
    }
    else
    {
        if (Input.Len() != HiddenWord.Len())
        {
            PrintLine(TEXT("Write a word with %i letters!"), HiddenWord.Len());
            return;
        }

        if (!IsIsogram(Input)) 
        {
            PrintLine(TEXT("Word %s is not an isogram!"), *Input);
            return;
        }

        if (LoseLife() <= 0)
        {
            LoseMatch();
        }
        else
        {
            PrintLine(TEXT("Bulls: %i"), CountBulls(Input));
            PrintLine(TEXT("Cows: %i"), CountCows(Input));
            PrintLine(TEXT("Lives: %i"), Lives);
        }
    }

}


